package ooss;

import java.util.Objects;

public class Student extends Person {
    private Klass klass;

    public Klass getKlass() {
        return klass;
    }

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public String introduce() {
        String introduction = super.introduce() + " I am a student.";
        introduction += (klass != null && klass.getLeader() != null && klass.getLeader().equals(this))
                ? String.format(" I am the leader of class %d.", klass.getNumber())
                : (klass != null)
                ? String.format(" I am in class %d.", klass.getNumber())
                : "";
        return introduction;
    }

    public void join(Klass klass) {
        this.klass = klass;
        klass.attach(this);
    }

    public boolean isIn(Klass klass) {
        return this.klass != null && this.klass.equals(klass);
    }

    @Override
    public void speak(Integer classNumber, String personName) {
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.", this.name, classNumber, personName);
    }
}
