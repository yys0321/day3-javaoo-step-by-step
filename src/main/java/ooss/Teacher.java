package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> klass = new ArrayList<>();

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    public String introduce() {
        String klassString = klass.stream()
                .map(klass -> String.valueOf(klass.getNumber()))
                .collect(Collectors.joining(", "));
        String introduction = super.introduce() + " I am a teacher.";
        introduction += !klassString.isEmpty()
                ? String.format(" I teach Class %s.", klassString)
                : "";
        return introduction;
    }

    public void assignTo(Klass klass) {
        this.klass.add(klass);
        klass.attach(this);
    }

    public boolean belongsTo(Klass klass) {
        return this.klass.stream().anyMatch(cur -> cur.equals(klass));
    }

    public boolean isTeaching(Student student) {
        return this.belongsTo(student.getKlass());
    }

    @Override
    public void speak(Integer classNumber, String personName) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.", this.name, classNumber, personName);
    }
}
