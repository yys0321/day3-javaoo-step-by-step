package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {
    private final int number;
    private Student leader;
    private List<Person> person = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
    public void assignLeader(Student student) {
        if(!student.isIn(this)) {
            System.out.println("It is not one of us.");
            return;
        }
        this.leader = student;
        this.person.forEach((person) -> person.speak(number, student.getName()));
    }

    public boolean isLeader(Student student) {
        if(this.leader == null) {
            return false;
        }
        return this.leader.equals(student);
    }

    public Student getLeader() {
        return leader;
    }

    public void attach(Person person) {
        this.person.add(person);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Klass klass = (Klass) object;
        return number == klass.number;
    }
}
